# Aquarium

Aquarium is an application made for easier mange aquarium tank.

![app](./docs/img/roomba_app.png)

### Requirements

Project is made with Qt framework in 5.15.x version. In order to install it check this [page](https://www.qt.io/download).

### License

Whole project is released under MIT License.

### Author

- Bartosz Wroński

### Acknowledgements

This project is part of thesis at _Wroclaw University of Science and Technology_ conducted by Wojciech Domski, BEng, PhD.
