/**
 * @file communication.hh
 * @author Jakub Kozłowicz (ja.kozlowicz@gmail.com)
 * @author Bartosz Wroński (bartek-wronski7@wp.pl)
 * @brief Communication class declaration.
 *
 * Communication class is a bridge between main application thread and thread
 * which read from serial port.
 *
 * @version 1.0
 * @date 2022-06-16
 *
 * @copyright Copyright (c) 2022
 */

#ifndef COMMUNICATION_HH
#define COMMUNICATION_HH

#include <QSerialPort>

#include "frameBuffer.hh"

/**
 * @brief The class connecting main application and serial port.
 *
 * Contain handle to the serial port device and circular buffer which store individual
 * frames from device.
 */
class Communication
{
    QSerialPort* _device = nullptr; ///< Serial device handle.
    FrameBuffer _frame;             ///< Circular buffer for frames.
    std::string _deviceName;        ///< Serial device name.
    bool _continue = true;          ///< Identifier whether to continue communication with serial device.
    bool _opened = false;           ///< Identifier whether serial device is open.

  public:
    /**
     * @brief Construct a new Communication object.
     *
     * Default constructor.
     */
    Communication() = default;

    /**
     * @brief Destroy the Communication object.
     *
     * Default destructor.
     */
    ~Communication() = default;

    /**
     * @brief Set the serial device name.
     *
     * @param[in] name New name of the serial device.
     */
    void setDeviceName(const std::string& name) { _deviceName = name; }

    /**
     * @brief Get the serial device name.
     *
     * @return Name of the serial device.
     */
    const std::string& getDeviceName() const { return _deviceName; }

    /**
     * @brief Set a new handle to the serial device.
     *
     * @param[in] device Handle to a new serial device.
     */
    void setDevice(QSerialPort* device) { _device = device; }

    /**
     * @brief Open the serial port.
     *
     * Set the serial port parameters and open it in read only mode.
     *
     * @param[in] serialPort The serial port name.
     * @return Identifier wether the serial port was opened successfully.
     * @retval true Serial port opened successfully.
     * @retval false Serial port opened incorrectly.
     */
    bool openSerialPort(const char* serialPort);

    /**
     * @brief Wrapper function for opening a serial port.
     * @see openSerialPort
     */
    bool openSerialPort() { return openSerialPort(_deviceName.c_str()); }

    /**
     * @brief Close the serial port.
     * @return Identifier wether the serial port was closed successfully.
     * @retval true Serial port closed successfully.
     * @retval false Serial port closed incorrectly.
     */
    bool closeSerialPort();

    /**
     * @brief Check if the serial port is open.
     * @return Identifier wether the serial port is open.
     * @retval true Serial port is open.
     * @retval false Serial port is closed.
     */
    bool isOpen() const { return _opened; }

    /**
     * @brief Get a modifiable frame from the circular buffer.
     * @return Mutable reference to the one frame from buffer.
     */
    FrameBuffer& useFrameBuffer() { return _frame; }

    /**
     * @brief Get a unmodifiable frame from the circular buffer.
     * @return Immutable reference to the one frame from buffer.
     */
    const FrameBuffer& getFrameBuffer() const { return _frame; }

    /**
     * @brief Check wether to continue communication with the serial device.
     * @return Identifier wether to continue communication.
     * @retval true Continue communication.
     * @retval false End communication.
     */
    bool toContinue() const { return _continue; }

    /**
     * @brief Set the end of communication flag to end communication.
     */
    void endCommunication() { _continue = false; }

    /**
     * @brief Receive line one by one from the serial device in loop until end of communication.
     */
    void receiveData();



    void sendData(const QString& message);
};

#endif /* COMMUNICATION_HH */
