/**
 * @file mainwindow.hh
 * @author Jakub Kozłowicz (ja.kozlowicz@gmail.com)
 * @author Bartosz Wroński (bartek-wronski7@wp.pl)
 * @brief MainWindow class declaration.
 *
 * MainWindow is main application class which connect graphical interface with logic of application.
 *
 * @version 0.1
 * @date 2022-06-16
 *
 * @copyright Copyright (c) 2022
 */

#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <QGroupBox>
#include <QMainWindow>
#include <QProgressBar>
#include <QSerialPort>
#include <QTimer>
#include <QTranslator>
#include <QTime>

#include "communication.hh"
#include "frameThread.hh"
#include "parser.hh"


#define MAX_HEATER_POWER 18.00



QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

/**
 * @brief The class connecting all modules through the UI interface.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    /**
     * @brief Construct a new MainWindow object.
     * @param[in, out] parent Pointer to the parent QWidget object.
     */
    MainWindow(QWidget* parent = nullptr);


    /**
     * @brief Destroy the MainWindow object.
     *
     * Deallocate all dynamic memory.
     */
    ~MainWindow();

  protected:
    /**
     * @brief Closing event function.
     *
     * Reimplemented to ensure that the serial port was properly closed.
     *
     * @param[in] event Closing event.
     */
    void closeEvent(QCloseEvent* event) override;

  private slots:
    /**
     * @brief Slot triggered when the closing signal was emitted.
     */
    void on_actionClose_triggered();

    /**
     * @brief Slot triggered when the disconnecting signal was emitted.
     */
    void on_actionDisconnect_triggered();

    /**
     * @brief Slot triggered when the searching signal was emitted.
     */
    void on_actionSearch_triggered();

    /**
     * @brief Connect to the serial device.
     *
     * Slot triggered when the connect signal was emmitted.
     *
     * @param[in] actionName Name of the action that have been clicked (action name represents device name).
     */
    void connectSerialDevice(const QString& actionName);

    /**
     * @brief Update information read from the serial port.
     *
     * Slot triggered when the timer run out.
     */
    void serialTimeout();

    /**
     * @brief Show error message as status bar message.
     * @param[in] message Error message string.
     */
    void showErrorMessage(const QString& message);

    /**
     * @brief Add a message to the logs widget.
     * @param[in] message Info message string.
     */
    void addMessageToLogs(const QString& message);




    void addHeaterPower();
    void addHeaterState(const QString& message);
    void addTempSet();
    void addTempCurrent(const QString& message);
    void addPowerSource(const QString& message);
    void addBaterryVoltage(const QString& message);
    void addBaterryCharge(const QString& message);
    void addLightSet();
    void addLightCurrent(const QString& message);
    void addWaterLevel(const QString& message);

    /**
     * @brief Set the connection status with the serial port on the status bar.
     * @param[in] message Serial status message.
     */

    void changeHeaterOn();
    void changeHeaterOff();
    void changeHeaterAuto();
    void changeHeaterManual();

    void changeLightOn();
    void changeLightOff();
    void changeLightAuto();
    void changeLightManual();

    void changeFilterOn();
    void changeFilterOff();


    void setSerialStatus(const QString& message);
    void sendData();



private:
    /**
     * @brief Add drop shadow under given GroupBox widget.
     * @param[in] group The handle of the groupbox widget to which the effect is to be added.
     * @param[in] xOffset Shadow offset on the x axis.
     * @param[in] yOffset Shadow offset on the y axis.
     * @param[in] radius Shadow radius.
     */
    void addDropShadow(QGroupBox* group, int xOffset, int yOffset, int radius);


    Ui::MainWindow* ui;            ///< Pointer to all UI widgets.
    Communication* _communication; ///< Communication handle.
    QTimer _serialTimer;           ///< Serial data updating timer.
    FrameThread* _frameThread;     ///< Handle for communicating on different thread.
    Parser* _parser;               ///< Serial data processor.
    QTranslator _translator;       ///< Handle to translator object.

    int _hour_on;
    int _minute_on;
    int _hour_off;
    int _minute_off;

};

#endif /* MAINWINDOW_HH */
