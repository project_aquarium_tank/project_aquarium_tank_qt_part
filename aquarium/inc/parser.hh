/**
 * @file parser.hh
 * @author Bartosz Wroński (bartek-wronski7@wp.pl)
 * @author Jakub Kozłowicz (ja.kozlowicz@gmail.com)
 * @brief Parser class declaration.
 * @version 1.0
 * @date 2022-06-06
 *
 * @copyright Copyright (c) 2022
 */

#ifndef PARSER_HH
#define PARSER_HH

#include <QObject>


#define CRC_INIT 0xffff
#define CRC_POLYNOMIAL 0x1021

#define MIN_BATERRY_VOL 10.5
#define MAX_BATERRY_VOL 13.4
#define MAX_SLIDER_VALUE 100
/**
 * @brief Parsing class for a processing data from the serial port frame.
 */
class Parser : public QObject
{
    Q_OBJECT

  public:
    /**
     * @brief Construct a new Parser object.
     */
    explicit Parser() = default;

    /**
     * @brief Destroy the Parser object.
     */
    ~Parser() = default;

    /**
     * @brief Parse line from the serial port into managed variables.
     * @param[in] frame One line from the serial port.
     * @return Identifier wether a frame was parsed successfully.
     */
    bool parseFrame(const std::string& frame);

    /**
     * @brief Process data from a one frame.
     */
    void processData();

  signals:

    void changedHeaterState(const QString& HeaterState);
    void changedTempCurrent(const QString& TempCurrent );
    void changedPowerSource(const QString& PowerSource );
    void changedBaterryVoltage(const QString& BaterryVoltage );
    void changedBaterryLevel(const QString& BaterryLevel );
    void changedLightCurrent(const QString& LightCurrent );
    void changedWaterLevel(const QString& WaterLevel);


  private:


    float power_heater;
    int state_heater;
    float measured_temp;
    float measured_lux;
    float measured_water_level;
    int power_source;
    float baterry_voltage;

    int _CRC16;                      ///< Sent CRC from the device.
};

#endif /* PARSER_HH */
