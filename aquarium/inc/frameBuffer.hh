/**
 * @file frameBuffer.hh
 * @author Jakub Kozłowicz (ja.kozlowicz@gmail.com)
 * @author Bartosz Wroński (bartek-wronski7@wp.pl)
 * @brief FrameBuffer class declaration.
 *
 * FrameBuffer class implements circular buffer with data overwritten when size
 * of this buffer is to big. It allows to read from the serial device on a different
 * thread.
 *
 * @version 1.0
 * @date 2022-06-16
 *
 * @copyright Copyright (c) 2022
 */

#ifndef FRAMEBUFFER_HH
#define FRAMEBUFFER_HH

#include <list>
#include <mutex>
#include <string>
#include <thread>

/**
 * @brief Class implementing circular buffer.
 */
class FrameBuffer
{
    std::list<std::string> _frameList;        ///< List containing individual frames.
    mutable std::mutex _access;               ///< Mutex that provides single access to the list.
    static constexpr int _MAX_LIST_SIZE = 10; ///< Max circular buffer size.

  public:
    /**
     * @brief Construct a new FrameBuffer object.
     *
     * Default constructor.
     */
    FrameBuffer() = default;

    /**
     * @brief Destroy the FrameBuffer object.
     *
     * Default destructor.
     */
    ~FrameBuffer() = default;

    /**
     * @brief Add one frame at the end of list.
     * @param[in] frame One line of data from the serial device.
     */
    void addFrame(const std::string& frame);

    /**
     * @brief Get one frame from the top of the list.
     * @param[out] frame String to which frame will be saved.
     * @return Identifier wether the frame was received.
     * @retval true Frame successfully received.
     * @retval false Frame list is empty. Can not receive any frame.
     */
    bool getFrame(std::string& frame);

    /**
     * @brief Check if there is any frame in a circular buffer.
     * @return Identifier wether the list is empty.
     * @retval true There are at least one frame in the list.
     * @retval false The list is empty.
     */
    bool isFrame() const;
};

#endif /* FRAMEBUFFER_HH */
