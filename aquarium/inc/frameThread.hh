/**
 * @file frameThread.hh
 * @author Jakub Kozłowicz (ja.kozlowicz@gmail.com)
 * @author Bartosz Wroński (bartek-wronski7@wp.pl)
 * @brief FrameThread class declaration.
 *
 * FrameThread class is wrapper class for QThread to communicate with the serial device on a separate thread.
 *
 * @version 1.0
 * @date 2022-06-16
 *
 * @copyright Copyright (c) 2022
 *
 */

#ifndef FRAMETHREAD_HH
#define FRAMETHREAD_HH

#include <QThread>

#include "communication.hh"

/**
 * @brief Thread wrapper class for communicating with the serial device on a separate thread.
 */
class FrameThread : public QThread
{
    Q_OBJECT
    Communication* _communication = nullptr; ///< Communication class handle.

  public:
    /**
     * @brief Construct a new FrameThread object.
     * @param[in] communication Communication class handle.
     * @param[in, out] parent Pointer to the parent object.
     */
    FrameThread(Communication* communication = nullptr, QObject* parent = nullptr);

    /**
     * @brief Run main thread function.
     */
    void run() override;

  signals:
    /**
     * @brief Signal emmitted to add informations to logs.
     * @param[out] info Info message string.
     */
    void info(const QString& info);

    /**
     * @brief Signal emmitted to add errors on the status bar.
     * @param[out] error Error message string.
     */
    void error(const QString& error);

    /**
     * @brief Signal emmitted to change connection status with the serial device on the status bar.
     * @param[out] message Serial status message.
     */
    void setSerialStatus(const QString& message);
};

#endif /* FRAMETHREAD_HH */
