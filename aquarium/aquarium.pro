QT       += core gui serialport xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
CONFIG += file_copies

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/communication.cpp \
    src/frameBuffer.cpp \
    src/frameThread.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/parser.cpp

HEADERS += \
    inc/communication.hh \
    inc/frameBuffer.hh \
    inc/frameThread.hh \
    inc/mainwindow.hh \
    inc/parser.hh

FORMS += \
    ui/mainwindow.ui



CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = $$[QT_INSTALL_EXAMPLES]/charts/customchart /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    .clang-format \


INCLUDEPATH+=inc

RESOURCES += \
    res/resources.qrc

LIBS +=  #-lSOIL #-lQGLViewer-qt5



RC_ICONS = icon1.ico
