/**
 * @file frameThread.cpp
 * @author Jakub Kozłowicz (ja.kozlowicz@gmail.com)
 * @author Bartosz Wroński (bartek-wronski7@wp.pl)
 * @brief FrameThread class methods definitions.
 *
 * FrameThread class is wrapper class for QThread to communicate with serial device on separate thread.
 *
 * @version 1.0
 * @date 2022-06-16
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <QSerialPort>

#include "communication.hh"
#include "frameThread.hh"

FrameThread::FrameThread(Communication* communication /* = nullptr */, QObject* parent /* = nullptr */)
    : QThread(parent), _communication(communication)
{
}

void FrameThread::run()
{
    QSerialPort serial;
    _communication->setDevice(&serial);

    if(!_communication->openSerialPort())
    {
        emit error(tr("Wątek nie mógł otworzyć portu"));
        return;
    }

    emit info(tr("Otwarto port szeregowy"));
    emit setSerialStatus(tr("Połączono z portem szeregowym   |   Nazwa portu:  ") +
                         QString(_communication->getDeviceName().c_str()));

    _communication->receiveData();

    if(!_communication->closeSerialPort())
    {
        emit error(tr("Wątek nie mógł poprawnie zamknąć portu!"));
        return;
    }

    emit info(tr("Zamknięto połączenie."));
    emit setSerialStatus(tr("Rozłączono z portem szeregowym"));
}
