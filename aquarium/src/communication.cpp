/**
 * @file communication.cpp
 * @author Jakub Kozłowicz (ja.kozlowicz@gmail.com)
 * @author Bartosz Wroński (bartek-wronski7@wp.pl)
 * @brief Communication class methods definitions.
 *
 * Communication class is a bridge between main application thread and thread
 * which read from serial port.
 *
 * @version 1.0
 * @date 2022-06-16
 *
 * @copyright Copyright (c) 2022
 */

#include <QByteArray>
#include <QString>

#include "communication.hh"
#include "qdebug.h"

bool Communication::openSerialPort(const char* serialPort)
{
    _device = new QSerialPort();
    _device->setBaudRate(QSerialPort::Baud115200);
    _device->setFlowControl(QSerialPort::NoFlowControl);
    _device->setStopBits(QSerialPort::OneStop);
    _device->setDataBits(QSerialPort::Data8);
    _device->setParity(QSerialPort::NoParity);
    _device->setPortName(serialPort);

    if(_device->open(QSerialPort::ReadWrite))
    {
        _opened = true;
        return true;
    }
    else
    {
        return false;
    }
}

bool Communication::closeSerialPort()
{
    if(isOpen())
    {
        _device->close();
        _opened = false;
        return true;
    }
    else
    {
        return false;
    }
}

void Communication::receiveData()
{
    while(_continue)
    {
        while(_device->waitForReadyRead())
        {
            while(_device->canReadLine())
            {
                if(!_continue) return;
                QByteArray line = _device->readLine();
                if(line[line.length() - 1] == '\n')
                {
                    line[line.length() - 1] = 0;
                }
                if(line[line.length() - 2] == '\r')
                {
                    line[line.length() - 2] = 0;
                }
                _frame.addFrame(line.toStdString());
            }
        }
    }
}

void Communication::sendData(const QString& message)
{

        if(_device->isOpen()&&_device->isWritable())
        {
            qDebug()<< "Wysłano dane: " << message;
            _device->write(message.toStdString().c_str());
        }else
        {
            qDebug()<<"Nie wysłano danych.";
        }

}
