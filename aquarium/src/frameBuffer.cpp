/**
 * @file frameBuffer.cpp
 * @author Jakub Kozłowicz (ja.kozlowicz@gmail.com)
 * @author Bartosz Wroński (bartek-wronski7@wp.pl)
 * @brief FrameBuffer class methods definitions.
 *
 * FrameBuffer class implements circular buffer with data overwritten when size
 * of this buffer is to big. It allows to read from serial device on different
 * thread.
 *
 * @version 1.0
 * @date 2022-06-16
 *
 * @copyright Copyright (c) 2022
 */

#include "frameBuffer.hh"

void FrameBuffer::addFrame(const std::string& frame)
{
    std::lock_guard<std::mutex> guard(_access);

    _frameList.push_back(frame);
    while(_frameList.size() > _MAX_LIST_SIZE)
    {
        _frameList.pop_front();
    }
}

bool FrameBuffer::getFrame(std::string& frame)
{
    std::lock_guard<std::mutex> guard(_access);

    if(_frameList.empty()) return false;

    frame = _frameList.front();
    _frameList.pop_front();

    return true;
}

bool FrameBuffer::isFrame() const
{
    std::lock_guard<std::mutex> guard(_access);
    return !_frameList.empty();
}
