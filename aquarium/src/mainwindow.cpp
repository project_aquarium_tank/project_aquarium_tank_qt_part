﻿/**
 * @file mainwindow.cpp
 * @author Jakub Kozłowicz (ja.kozlowicz@gmail.com)
 * @author Bartosz Wroński (bartek-wronski7@wp.pl)
 * @brief MainWindow class methods definitions.
 *
 * MainWindow is main application class which connect graphical interface with logic of application.
 *
 * @version 0.1
 * @date 2022-06-16
 *
 * @copyright Copyright (c) 2022
 */

#include <QCloseEvent>
#include <QColor>
#include <QDateTime>
#include <QDebug>
#include <QGraphicsDropShadowEffect>
#include <QList>
#include <QObject>
#include <QSerialPortInfo>
#include <QSignalMapper>
#include <QTranslator>
#include <QTime>
#include <iomanip>


#include "mainwindow.hh"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::MainWindow), _communication(new Communication),
      _frameThread(new FrameThread(_communication, this)), _parser(new Parser())
{
    //QTimer *_interval_timer = new QTimer(this);

    ui->setupUi(this);
    ui->serialStatus->setAlignment(Qt::AlignCenter);
    ui->statusbar->addPermanentWidget(ui->serialStatus, 1);

    _serialTimer.setInterval(1000);
    _serialTimer.setSingleShot(true);

    connect(&_serialTimer, &QTimer::timeout, this, &MainWindow::serialTimeout);



    connect(_frameThread, &FrameThread::error, this, &MainWindow::showErrorMessage);
    connect(_frameThread, &FrameThread::info, this, &MainWindow::addMessageToLogs);
    connect(_frameThread, &FrameThread::setSerialStatus, this, &MainWindow::setSerialStatus);


    connect(_parser, &Parser::changedBaterryLevel, this, &MainWindow::addBaterryCharge);
    connect(_parser, &Parser::changedBaterryVoltage, this, &MainWindow::addBaterryVoltage);
    connect(_parser, &Parser::changedWaterLevel, this, &MainWindow::addWaterLevel);
    connect(_parser, &Parser::changedHeaterState, this, &MainWindow::addHeaterState);
    connect(_parser, &Parser::changedLightCurrent, this, &MainWindow::addLightCurrent);
    connect(_parser, &Parser::changedPowerSource, this, &MainWindow::addPowerSource);
    connect(_parser, &Parser::changedTempCurrent, this, &MainWindow::addTempCurrent);

    connect(ui->horizontalSliderTemperature, &QSlider::valueChanged,this,&MainWindow::addTempSet);
    connect(ui->horizontalSliderHeaterPower, &QSlider::valueChanged,this,&MainWindow::addHeaterPower);
    connect(ui->horizontalSliderLightValue, &QSlider::valueChanged, this,&MainWindow::addLightSet);

    connect(ui->pushButtonFilterOn, &QPushButton::released, this, &MainWindow::changeFilterOn);
    connect(ui->pushButtonFilterOff, &QPushButton::released, this, &MainWindow::changeFilterOff);

    connect(ui->pushButtonHeaterOn, &QPushButton::clicked, this, &MainWindow::changeHeaterOn);
    connect(ui->pushButtonHeaterOff, &QPushButton::clicked, this, &MainWindow::changeHeaterOff);
    connect(ui->pushButtonHeaterAuto, &QPushButton::clicked, this, &MainWindow::changeHeaterAuto);
    connect(ui->pushButtonHeaterManual, &QPushButton::clicked, this, &MainWindow::changeHeaterManual);

    connect(ui->pushButtonLightOn, &QPushButton::clicked, this, &MainWindow::changeLightOn);
    connect(ui->pushButtonLightOff, &QPushButton::clicked, this, &MainWindow::changeLightOff);
    connect(ui->pushButtonLightAuto, &QPushButton::clicked, this, &MainWindow::changeLightAuto);
    connect(ui->pushButtonLightManual, &QPushButton::clicked, this, &MainWindow::changeLightManual);

    connect(&_serialTimer, &QTimer::timeout, this, &MainWindow::sendData);

    ui->lineEditWaterLevel->setText("######");
    ui->lineEditBaterryVoltage->setText("######");
    ui->lineEditCurrentLight->setText("######");
    ui->lineEditSetLight->setText("0");
    ui->lineEditPowerSource->setText("######");
    ui->lineEditHeaterState->setText("######");
    ui->lineEditHeaterPower->setText("0.00");
    ui->lineEditTempCurrent->setText("######");
    ui->lineEditTempSet->setText(QString::number(MAX_HEATER_POWER));
    ui->progressBarBattery->setValue(0);

    ui->lineEditFilterWork->setText("Off");
    ui->lineEditHeaterWork->setText("Off");
    ui->lineEditLightWork->setText("Off");
    ui->lineEditHeaterMode->setText("Manual");
    ui->lineEditLightMode->setText("Manual");

    //_interval_timer->start(100);
    //connect( _interval_timer, &QTimer::timeout, this, &MainWindow::addTimeCurrent);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    _communication->endCommunication();
    _frameThread->wait();
    event->accept();
}

void MainWindow::on_actionClose_triggered()
{
    _communication->endCommunication();
    _frameThread->wait();
    QApplication::quit();
}

void MainWindow::on_actionDisconnect_triggered()
{
    _communication->endCommunication();
    _frameThread->wait();
}

void MainWindow::on_actionSearch_triggered()
{
    ui->menuConnect->clear();
    addMessageToLogs(tr("Szukam urządzeń..."));

    QList<QSerialPortInfo> devices;
    devices = QSerialPortInfo::availablePorts();

    QSignalMapper* mapper = new QSignalMapper;
    connect(mapper, SIGNAL(mappedString(QString)), this, SLOT(connectSerialDevice(QString)));

    for(int i = 0; i < devices.count(); i++)
    {
        QString message = tr("Znalazłem urządzenie: ");
        addMessageToLogs(message + devices.at(i).portName() + " " + devices.at(i).description());
        QAction* deviceName = new QAction(QString(devices.at(i).portName() + "\t" + devices.at(i).description()), this);
        QObject::connect(deviceName, SIGNAL(triggered()), mapper, SLOT(map()));
        mapper->setMapping(deviceName, deviceName->text());
        ui->menuConnect->addAction(deviceName);
    }
}

void MainWindow::connectSerialDevice(const QString& actionName)
{
    QString portName = actionName.split("\t").first();

    _communication->setDeviceName(portName.toStdString().data());

    if(!_communication->isOpen())
    {
        _frameThread->start();
        _serialTimer.start();
    }
    else
    {
        addMessageToLogs(tr("Port już jest otwarty!"));
        return;
    }
}

void MainWindow::serialTimeout()
{
    std::string line;

    while(_communication->getFrameBuffer().isFrame())
    {
        QString tmp_line = QString::fromStdString(line);
        addMessageToLogs(tmp_line);

        if(!_communication->useFrameBuffer().getFrame(line)) continue;

        if(_parser->parseFrame(line))
        {
            _parser->processData();
        }
        else
        {
            addMessageToLogs(tr("Niepoprawna ramka danych"));
        }
    }

    _serialTimer.start();
}

void MainWindow::showErrorMessage(const QString& message)
{
    ui->statusbar->showMessage(message, 100);
}

void MainWindow::addMessageToLogs(const QString& message)
{
    QString currentTime = QDateTime::currentDateTime().toString("hh:mm:ss");
    ui->textEditLogs->append(currentTime + " " + message);
}

void MainWindow::addDropShadow(QGroupBox* group, int xOffset, int yOffset, int radius)
{
    QGraphicsDropShadowEffect* shadow = new QGraphicsDropShadowEffect();
    shadow->setBlurRadius(radius);
    shadow->setOffset(xOffset, yOffset);
    group->setGraphicsEffect(shadow);
}



void MainWindow::addHeaterPower()
{
    int value_power = ui->horizontalSliderHeaterPower->value();
    float calc_power= (float)value_power*MAX_HEATER_POWER/ui->horizontalSliderHeaterPower->maximum();

    char buffer[5];

    sprintf(buffer,"%4.2f",calc_power);
    QString value(buffer);


    ui->lineEditHeaterPower->setText(value);
}


void MainWindow::addHeaterState(const QString& message)
{
    if(message=="1")
       ui->lineEditHeaterState->setText("On");
    else
       ui->lineEditHeaterState->setText("Off");
}


void MainWindow::addTempSet()
{
       int value_temp = ui->horizontalSliderTemperature->value();
       float calc_temperature= value_temp*0.014+18.0;


       char buffer[5];

       sprintf(buffer,"%04.2f",calc_temperature);
       QString value(buffer);

       ui->lineEditTempSet->setText(value);
}

void MainWindow::addTempCurrent(const QString& message)
{
       ui->lineEditTempCurrent->setText(message);
}

void MainWindow::addPowerSource(const QString& message)
{
    if(message=="1")
    {
       ui->lineEditPowerSource->setText("Sieć");
    }else
    {
       ui->lineEditPowerSource->setText("Akumulator");
    }


}

void MainWindow::addBaterryVoltage(const QString& message)
{
       ui->lineEditBaterryVoltage->setText(message);
}

void MainWindow::addBaterryCharge(const QString& message)
{
       float percent_baterry= message.toFloat();
       ui->progressBarBattery->setValue((int)percent_baterry);
}

void MainWindow::addLightSet()
{
       int value_lx = ui->horizontalSliderLightValue->value();
       char buffer[5];

       sprintf(buffer,"%4d",value_lx);
       QString value(buffer);

       ui->lineEditSetLight->setText(value);
}

void MainWindow::addLightCurrent(const QString& message)
{
       ui->lineEditCurrentLight->setText(message);
}


void MainWindow::addWaterLevel(const QString& message)
{
       ui->lineEditWaterLevel->setText(message);
}




void MainWindow::setSerialStatus(const QString& message)
{
    ui->serialStatus->setText(message);
}


void MainWindow::changeHeaterOn()
{
    QString message = "On";
    ui->lineEditHeaterWork->setText(message);
}


void MainWindow::changeHeaterOff()
{
    QString message = "Off";
    ui->lineEditHeaterWork->setText(message);
}


void MainWindow::changeHeaterAuto()
{
    QString message = "Auto";
    ui->lineEditHeaterMode->setText(message);
}


void MainWindow::changeHeaterManual()
{
    QString message = "Manual";
    ui->lineEditHeaterMode->setText(message);
}


void MainWindow::changeLightOn()
{
    QString message = "On";
    ui->lineEditLightWork->setText(message);
}


void MainWindow::changeLightOff()
{
    QString message = "Off";
    ui->lineEditLightWork->setText(message);
}


void MainWindow::changeLightAuto()
{
    QString message = "Auto";
    ui->lineEditLightMode->setText(message);
}


void MainWindow::changeLightManual()
{
    QString message = "Manual";
    ui->lineEditLightMode->setText(message);
}


void MainWindow::changeFilterOn()
{
    QString message = "On";
    ui->lineEditFilterWork->setText(message);
}


void MainWindow::changeFilterOff()
{
    QString message = "Off";
    ui->lineEditFilterWork->setText(message);
}


void MainWindow::sendData()
{
     std::string data;
     char buffer_time[6];

     const std::time_t now = std::time(nullptr);
     const std::tm calendar_time = *std::localtime( std::addressof(now) ) ;







     sprintf(buffer_time,"%5d",ui->horizontalSliderHeaterPower->value());
     std::string heater_power(buffer_time);

     //sprintf(buffer_time,"%4.2f",ui->lineEditTempSet->text().toFloat());
     sprintf(buffer_time,"%4d",ui->horizontalSliderTemperature->value());
     std::string temp_set(buffer_time);

     sprintf(buffer_time,"%4d",ui->horizontalSliderLightPower->value());
     std::string light_power(buffer_time);
     QString light_set = ui->lineEditSetLight->text();


     sprintf(buffer_time,"%2d",ui->timeEditOn->time().hour());
     std::string hour_on(buffer_time);

     sprintf(buffer_time,"%2d",ui->timeEditOn->time().minute());
     std::string minutes_on(buffer_time);

     sprintf(buffer_time,"%2d",ui->timeEditOff->time().hour());
     std::string hour_off(buffer_time);

     sprintf(buffer_time,"%2d",ui->timeEditOff->time().minute());
     std::string minutes_off(buffer_time);

     sprintf(buffer_time,"%2d",calendar_time.tm_hour);
     std::string hour_actual = (buffer_time);

     sprintf(buffer_time,"%2d",calendar_time.tm_min);
     std::string minutes_actual(buffer_time);


     std::string light_mode;
     std::string light_work;

     std::string filter_work;

     std::string heater_mode;
     std::string heater_work;


    if(ui->lineEditLightWork->text()=="On")
        light_work="1";
    else
        light_work="0";


    if(ui->lineEditLightMode->text()=="Auto")
        light_mode="1";
    else
        light_mode="0";


    if(ui->lineEditFilterWork->text()=="On")
        filter_work="1";
    else
        filter_work="0";


    if(ui->lineEditHeaterWork->text()=="On")
        heater_work="1";
    else
        heater_work="0";


    if(ui->lineEditHeaterMode->text()=="Auto")
        heater_mode="1";
    else
        heater_mode="0";


     data = "X " + heater_power + " " + temp_set + " " + light_power + " " + light_set.toStdString()
          + " " + hour_on + " " + minutes_on + " " + hour_off + " " + minutes_off
          + " " + hour_actual + " " + minutes_actual + " "+ light_mode + " " + light_work + " " + filter_work + " " + heater_mode
          + " " + heater_work + " ";



      const char* buffer = data.c_str();
      int dataLength = strlen(data.c_str());
      //qDebug() << "Liczba znaków" << dataLength;
      unsigned short int CRC16 = CRC_INIT;
      while(dataLength--)
      {
          CRC16 ^= *(unsigned const char*)buffer++ << 8;
          for(int i = 0; i < 8; i++)
          {
              CRC16 = CRC16 & 0x8000 ? (CRC16 << 1) ^ CRC_POLYNOMIAL : CRC16 << 1;
          }
      }

      std::stringstream stream;
      stream << std::hex << CRC16;
      std::string hex_CRC16( stream.str() );

      data = data + hex_CRC16;

      const QString frame = QString::fromStdString(data);
      _communication->sendData(frame);
}






