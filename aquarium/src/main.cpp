/**
 * @file main.cpp
 * @author Jakub Kozłowicz <ja.kozlowicz@gmail.com>
 * @author Bartosz Wroński <bartek-wronski7@wp.pl>
 * @brief Application entry point
 * @version 1.0
 * @date 2022-05-05
 *
 * @copyright Copyright (c) 2022 Jakub Kozłowicz, Bartosz Wroński

 */

#include <QApplication>
#include <QDebug>
#include <QLocale>
#include <QTranslator>

#include "mainwindow.hh"

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon("./images/icon1.png"));
    QCoreApplication::setApplicationName(QString("Aquarium system"));

    MainWindow w;
    w.setWindowTitle(QCoreApplication::applicationName());
    w.show();

    return a.exec();
}
