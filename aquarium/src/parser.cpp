/**
 * @file parser.cpp
 * @author Bartosz Wroński (bartek-wronski7@wp.pl)
 * @author Jakub Kozłowicz (ja.kozlowicz@gmail.com)
 * @brief Parser class methods definitions.
 * @version 1.0
 * @date 2022-06-16
 *
 * @copyright Copyright (c) 2022
 */

#include <QByteArray>
#include <QDebug>
#include <QPoint>
#include <QTime>

#include <sstream>
#include <string>

#include "parser.hh"


bool Parser::parseFrame(const std::string& frame)
{
    char frameHeader;
    std::istringstream frameStream(frame);
    unsigned short int CRC16 = CRC_INIT;
    int frameLength = strlen(frame.c_str()); // length method counts \0 :(

    qDebug() << "Liczba odebranych danych "<< QString::fromStdString(frame); ;

    frameStream >> frameHeader;
    if(frameStream.fail() || frameHeader != 'X')
    {
        qDebug()<< QTime::currentTime().toString() << "Could not read frame identifier or is incorrect";
        frameStream.clear();
        return false;
    }

    frameStream >> state_heater;
    frameStream >> measured_temp >> measured_lux >> measured_water_level;
    frameStream >> power_source >> baterry_voltage;



    frameStream >> std::hex >> _CRC16;

    qDebug() << QTime::currentTime().toString() << frameHeader << state_heater << measured_temp << measured_lux << measured_water_level << power_source << baterry_voltage <<_CRC16;
    qDebug() << frameLength;

    const char* buffer = frame.c_str();
    int length = frameLength - 5; // At the end there is CRC16

    while(length--)
    {
        CRC16 ^= *(unsigned const char*)buffer++ << 8;
        for(int i = 0; i < 8; i++)
        {
            CRC16 = CRC16 & 0x8000 ? (CRC16 << 1) ^ CRC_POLYNOMIAL : CRC16 << 1;
        }
    }


    if(CRC16 != _CRC16)
    {
        qDebug() << "Calculated CRC16 different from read one";
        qDebug() << CRC16 << "\t"  << _CRC16 << Qt::endl;

        return false;
    }else
    {
       qDebug() << "Get correct frame";
    }

    return true;
}




void Parser::processData()
{
    QString value;

    value = QString::number(state_heater);
    emit changedHeaterState(value);

    value = QString::number(measured_temp);
    emit changedTempCurrent(value);

    value = QString::number(measured_lux);
    emit changedLightCurrent(value);

   value = QString::number(power_source);
    emit changedPowerSource(value);

    value = QString::number(baterry_voltage);

    emit changedBaterryVoltage(value);


    value= QString::number((baterry_voltage-MIN_BATERRY_VOL)/(MAX_BATERRY_VOL-MIN_BATERRY_VOL)* MAX_SLIDER_VALUE);
    emit changedBaterryLevel(value);

    value = QString::number(measured_water_level);

    emit changedWaterLevel(value);



}

