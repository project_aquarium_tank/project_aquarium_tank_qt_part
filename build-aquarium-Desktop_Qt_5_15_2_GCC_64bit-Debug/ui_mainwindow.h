/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QTimeEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionClose;
    QAction *actionDisconnect;
    QAction *actionSearch;
    QAction *actionPolish;
    QAction *actionEnglish_US;
    QWidget *centralwidget;
    QLabel *serialStatus;
    QGroupBox *groupBox;
    QGroupBox *groupBox_2;
    QSlider *horizontalSliderHeaterPower;
    QGroupBox *groupBox_3;
    QLineEdit *lineEditHeaterPower;
    QGroupBox *groupBox_4;
    QLineEdit *lineEditHeaterState;
    QGroupBox *groupBox_5;
    QGroupBox *groupBox_6;
    QLineEdit *lineEditTempSet;
    QGroupBox *groupBox_7;
    QLineEdit *lineEditTempCurrent;
    QGroupBox *groupBox_23;
    QSlider *horizontalSliderTemperature;
    QGroupBox *groupBox_8;
    QGroupBox *groupBox_9;
    QSlider *horizontalSliderLightPower;
    QGroupBox *groupBox_12;
    QGroupBox *groupBox_13;
    QLineEdit *lineEditSetLight;
    QGroupBox *groupBox_15;
    QGroupBox *groupBox_16;
    QTimeEdit *timeEditOn;
    QGroupBox *groupBox_17;
    QTimeEdit *timeEditOff;
    QGroupBox *groupBox_10;
    QLineEdit *lineEditCurrentTime;
    QGroupBox *groupBox_24;
    QSlider *horizontalSliderLightValue;
    QGroupBox *groupBox_14;
    QLineEdit *lineEditCurrentLight;
    QGroupBox *groupBox_20;
    QGroupBox *groupBox_44;
    QLineEdit *lineEditPowerSource;
    QGroupBox *groupBox_45;
    QLineEdit *lineEditBaterryVoltage;
    QGroupBox *groupBoxBatteryLevel;
    QGridLayout *gridLayout_8;
    QProgressBar *progressBarBattery;
    QGroupBox *groupBox_21;
    QPushButton *pushButtonLightAuto;
    QPushButton *pushButtonLightManual;
    QPushButton *pushButtonLightOn;
    QPushButton *pushButtonLightOff;
    QGroupBox *groupBox_22;
    QGroupBox *groupBox_28;
    QPushButton *pushButtonHeaterAuto;
    QPushButton *pushButtonHeaterManual;
    QPushButton *pushButtonHeaterOn;
    QPushButton *pushButtonHeaterOff;
    QGroupBox *groupBox_31;
    QPushButton *pushButtonFilterOn;
    QPushButton *pushButtonFilterOff;
    QGroupBox *groupBox_18;
    QGroupBox *groupBox_19;
    QLineEdit *lineEditWaterLevel;
    QGroupBox *groupBoxTextLogs;
    QGridLayout *gridLayout_5;
    QTextEdit *textEditLogs;
    QMenuBar *menubar;
    QMenu *menuFile;
    QMenu *menuConnection;
    QMenu *menuConnect;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1024, 540);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(1024, 540));
        MainWindow->setMaximumSize(QSize(1024, 540));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/icons/roomba.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-image: url(:/icons/icons/background.png);/*komentowac do pracy"));
        actionClose = new QAction(MainWindow);
        actionClose->setObjectName(QString::fromUtf8("actionClose"));
        actionDisconnect = new QAction(MainWindow);
        actionDisconnect->setObjectName(QString::fromUtf8("actionDisconnect"));
        actionSearch = new QAction(MainWindow);
        actionSearch->setObjectName(QString::fromUtf8("actionSearch"));
        actionPolish = new QAction(MainWindow);
        actionPolish->setObjectName(QString::fromUtf8("actionPolish"));
        actionEnglish_US = new QAction(MainWindow);
        actionEnglish_US->setObjectName(QString::fromUtf8("actionEnglish_US"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        centralwidget->setStyleSheet(QString::fromUtf8("QWidget#centralwidget {\n"
"	background-color: #C4C4C4;\n"
"}"));
        serialStatus = new QLabel(centralwidget);
        serialStatus->setObjectName(QString::fromUtf8("serialStatus"));
        serialStatus->setGeometry(QRect(9, 751, 231, 17));
        serialStatus->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        serialStatus->setTextFormat(Qt::PlainText);
        serialStatus->setAlignment(Qt::AlignCenter);
        serialStatus->setMargin(0);
        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 0, 661, 181));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setUnderline(false);
        font.setWeight(75);
        groupBox->setFont(font);
        groupBox->setAlignment(Qt::AlignCenter);
        groupBox->setFlat(false);
        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 30, 211, 71));
        groupBox_2->setAlignment(Qt::AlignCenter);
        horizontalSliderHeaterPower = new QSlider(groupBox_2);
        horizontalSliderHeaterPower->setObjectName(QString::fromUtf8("horizontalSliderHeaterPower"));
        horizontalSliderHeaterPower->setGeometry(QRect(10, 30, 191, 31));
        horizontalSliderHeaterPower->setStyleSheet(QString::fromUtf8(".QSlider {\n"
"    min-height: 12px;\n"
"    max-height: 68px;\n"
"    background: #060b0f ;\n"
"}\n"
"\n"
"\n"
".QSlider::groove:horizontal {\n"
"    border: 1px solid rgb(94, 92, 100);\n"
"    border-radius: 5px;\n"
"    height: 25px;\n"
"    background: rgb(26, 95, 180);\n"
"    margin: 0 12px;\n"
"}\n"
"\n"
".QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 24px;\n"
"    margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 3px;\n"
"}"));
        horizontalSliderHeaterPower->setMaximum(1000);
        horizontalSliderHeaterPower->setOrientation(Qt::Horizontal);
        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(210, 30, 181, 71));
        groupBox_3->setAlignment(Qt::AlignCenter);
        lineEditHeaterPower = new QLineEdit(groupBox_3);
        lineEditHeaterPower->setObjectName(QString::fromUtf8("lineEditHeaterPower"));
        lineEditHeaterPower->setGeometry(QRect(30, 30, 121, 27));
        sizePolicy.setHeightForWidth(lineEditHeaterPower->sizePolicy().hasHeightForWidth());
        lineEditHeaterPower->setSizePolicy(sizePolicy);
        lineEditHeaterPower->setMinimumSize(QSize(100, 10));
        lineEditHeaterPower->setMaximumSize(QSize(200, 30));
        QFont font1;
        font1.setPointSize(13);
        lineEditHeaterPower->setFont(font1);
        lineEditHeaterPower->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"	color:rgb(36, 31, 49);\n"
"    border: 2px solid gray;\n"
"    border-radius: 10px;\n"
"    height: 25px;\n"
"    padding: 0 8px;\n"
"    background: white;\n"
"    selection-background-color: darkgray;\n"
"}"));
        lineEditHeaterPower->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        lineEditHeaterPower->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEditHeaterPower->setReadOnly(true);
        groupBox_4 = new QGroupBox(groupBox);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(230, 100, 141, 71));
        groupBox_4->setAlignment(Qt::AlignCenter);
        lineEditHeaterState = new QLineEdit(groupBox_4);
        lineEditHeaterState->setObjectName(QString::fromUtf8("lineEditHeaterState"));
        lineEditHeaterState->setGeometry(QRect(10, 30, 121, 30));
        sizePolicy.setHeightForWidth(lineEditHeaterState->sizePolicy().hasHeightForWidth());
        lineEditHeaterState->setSizePolicy(sizePolicy);
        lineEditHeaterState->setMinimumSize(QSize(100, 10));
        lineEditHeaterState->setMaximumSize(QSize(200, 30));
        lineEditHeaterState->setFont(font1);
        lineEditHeaterState->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"	color:rgb(36, 31, 49);\n"
"    border: 2px solid gray;\n"
"    border-radius: 10px;\n"
"    padding: 0 8px;\n"
"    background: white;\n"
"    selection-background-color: darkgray;\n"
"}"));
        lineEditHeaterState->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        lineEditHeaterState->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEditHeaterState->setReadOnly(true);
        groupBox_5 = new QGroupBox(groupBox);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(380, 30, 271, 131));
        groupBox_5->setAlignment(Qt::AlignCenter);
        groupBox_6 = new QGroupBox(groupBox_5);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setGeometry(QRect(10, 30, 121, 61));
        groupBox_6->setAlignment(Qt::AlignCenter);
        lineEditTempSet = new QLineEdit(groupBox_6);
        lineEditTempSet->setObjectName(QString::fromUtf8("lineEditTempSet"));
        lineEditTempSet->setGeometry(QRect(10, 30, 101, 27));
        sizePolicy.setHeightForWidth(lineEditTempSet->sizePolicy().hasHeightForWidth());
        lineEditTempSet->setSizePolicy(sizePolicy);
        lineEditTempSet->setMinimumSize(QSize(100, 10));
        lineEditTempSet->setMaximumSize(QSize(200, 30));
        lineEditTempSet->setFont(font1);
        lineEditTempSet->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"    color:rgb(36, 31, 49);\n"
"    border: 2px solid gray;\n"
"    border-radius: 10px;\n"
"    padding: 0 8px;\n"
"    background: white;\n"
"    selection-background-color: darkgray;\n"
"}"));
        lineEditTempSet->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        lineEditTempSet->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEditTempSet->setReadOnly(true);
        groupBox_7 = new QGroupBox(groupBox_5);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        groupBox_7->setGeometry(QRect(140, 30, 121, 61));
        groupBox_7->setAlignment(Qt::AlignCenter);
        lineEditTempCurrent = new QLineEdit(groupBox_7);
        lineEditTempCurrent->setObjectName(QString::fromUtf8("lineEditTempCurrent"));
        lineEditTempCurrent->setGeometry(QRect(10, 30, 101, 27));
        sizePolicy.setHeightForWidth(lineEditTempCurrent->sizePolicy().hasHeightForWidth());
        lineEditTempCurrent->setSizePolicy(sizePolicy);
        lineEditTempCurrent->setMinimumSize(QSize(100, 10));
        lineEditTempCurrent->setMaximumSize(QSize(200, 30));
        lineEditTempCurrent->setFont(font1);
        lineEditTempCurrent->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"	color:rgb(36, 31, 49);\n"
"    border: 2px solid gray;\n"
"    border-radius: 10px;\n"
"    padding: 0 8px;\n"
"    background: white;\n"
"    selection-background-color: darkgray;\n"
"}"));
        lineEditTempCurrent->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        lineEditTempCurrent->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEditTempCurrent->setReadOnly(true);
        groupBox_23 = new QGroupBox(groupBox);
        groupBox_23->setObjectName(QString::fromUtf8("groupBox_23"));
        groupBox_23->setGeometry(QRect(10, 100, 211, 71));
        groupBox_23->setAlignment(Qt::AlignCenter);
        horizontalSliderTemperature = new QSlider(groupBox_23);
        horizontalSliderTemperature->setObjectName(QString::fromUtf8("horizontalSliderTemperature"));
        horizontalSliderTemperature->setGeometry(QRect(10, 30, 191, 31));
        horizontalSliderTemperature->setStyleSheet(QString::fromUtf8(".QSlider {\n"
"    min-height: 12px;\n"
"    max-height: 68px;\n"
"    background: #060b0f ;\n"
"}\n"
"\n"
"\n"
".QSlider::groove:horizontal {\n"
"    border: 1px solid rgb(94, 92, 100);\n"
"    border-radius: 5px;\n"
"    height: 25px;\n"
"    background: rgb(26, 95, 180);\n"
"    margin: 0 12px;\n"
"}\n"
"\n"
".QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 24px;\n"
"    margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 3px;\n"
"}"));
        horizontalSliderTemperature->setMaximum(1000);
        horizontalSliderTemperature->setOrientation(Qt::Horizontal);
        groupBox_8 = new QGroupBox(centralwidget);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        groupBox_8->setGeometry(QRect(10, 170, 781, 191));
        groupBox_8->setFont(font);
        groupBox_8->setAlignment(Qt::AlignCenter);
        groupBox_8->setFlat(false);
        groupBox_9 = new QGroupBox(groupBox_8);
        groupBox_9->setObjectName(QString::fromUtf8("groupBox_9"));
        groupBox_9->setGeometry(QRect(10, 30, 211, 71));
        groupBox_9->setAlignment(Qt::AlignCenter);
        horizontalSliderLightPower = new QSlider(groupBox_9);
        horizontalSliderLightPower->setObjectName(QString::fromUtf8("horizontalSliderLightPower"));
        horizontalSliderLightPower->setGeometry(QRect(10, 30, 191, 31));
        horizontalSliderLightPower->setStyleSheet(QString::fromUtf8(".QSlider {\n"
"    min-height: 12px;\n"
"    max-height: 68px;\n"
"    background: #060b0f ;\n"
"}\n"
"\n"
"\n"
".QSlider::groove:horizontal {\n"
"    border: 1px solid rgb(94, 92, 100);\n"
"    border-radius: 5px;\n"
"    height: 25px;\n"
"    background: rgb(26, 95, 180);\n"
"    margin: 0 12px;\n"
"}\n"
"\n"
".QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 24px;\n"
"    margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 3px;\n"
"}"));
        horizontalSliderLightPower->setOrientation(Qt::Horizontal);
        groupBox_12 = new QGroupBox(groupBox_8);
        groupBox_12->setObjectName(QString::fromUtf8("groupBox_12"));
        groupBox_12->setGeometry(QRect(230, 30, 151, 151));
        groupBox_12->setAlignment(Qt::AlignCenter);
        groupBox_13 = new QGroupBox(groupBox_12);
        groupBox_13->setObjectName(QString::fromUtf8("groupBox_13"));
        groupBox_13->setGeometry(QRect(0, 20, 141, 71));
        groupBox_13->setAlignment(Qt::AlignCenter);
        lineEditSetLight = new QLineEdit(groupBox_13);
        lineEditSetLight->setObjectName(QString::fromUtf8("lineEditSetLight"));
        lineEditSetLight->setGeometry(QRect(10, 30, 115, 30));
        sizePolicy.setHeightForWidth(lineEditSetLight->sizePolicy().hasHeightForWidth());
        lineEditSetLight->setSizePolicy(sizePolicy);
        lineEditSetLight->setMinimumSize(QSize(100, 10));
        lineEditSetLight->setMaximumSize(QSize(115, 30));
        lineEditSetLight->setFont(font1);
        lineEditSetLight->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"	color:rgb(36, 31, 49);\n"
"    border: 2px solid gray;\n"
"    border-radius: 10px;\n"
"    padding: 0 8px;\n"
"    background: white;\n"
"    selection-background-color: darkgray;\n"
"}"));
        lineEditSetLight->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        lineEditSetLight->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEditSetLight->setReadOnly(true);
        groupBox_15 = new QGroupBox(groupBox_8);
        groupBox_15->setObjectName(QString::fromUtf8("groupBox_15"));
        groupBox_15->setGeometry(QRect(390, 40, 381, 121));
        groupBox_15->setAlignment(Qt::AlignCenter);
        groupBox_16 = new QGroupBox(groupBox_15);
        groupBox_16->setObjectName(QString::fromUtf8("groupBox_16"));
        groupBox_16->setGeometry(QRect(10, 30, 101, 71));
        groupBox_16->setAlignment(Qt::AlignCenter);
        timeEditOn = new QTimeEdit(groupBox_16);
        timeEditOn->setObjectName(QString::fromUtf8("timeEditOn"));
        timeEditOn->setGeometry(QRect(7, 30, 91, 31));
        timeEditOn->setStyleSheet(QString::fromUtf8("color:rgb(36, 31, 49);"));
        groupBox_17 = new QGroupBox(groupBox_15);
        groupBox_17->setObjectName(QString::fromUtf8("groupBox_17"));
        groupBox_17->setGeometry(QRect(120, 30, 111, 71));
        groupBox_17->setAlignment(Qt::AlignCenter);
        timeEditOff = new QTimeEdit(groupBox_17);
        timeEditOff->setObjectName(QString::fromUtf8("timeEditOff"));
        timeEditOff->setGeometry(QRect(10, 30, 91, 31));
        timeEditOff->setStyleSheet(QString::fromUtf8("color:rgb(36, 31, 49);\n"
""));
        groupBox_10 = new QGroupBox(groupBox_15);
        groupBox_10->setObjectName(QString::fromUtf8("groupBox_10"));
        groupBox_10->setGeometry(QRect(240, 30, 131, 71));
        groupBox_10->setAlignment(Qt::AlignCenter);
        lineEditCurrentTime = new QLineEdit(groupBox_10);
        lineEditCurrentTime->setObjectName(QString::fromUtf8("lineEditCurrentTime"));
        lineEditCurrentTime->setGeometry(QRect(10, 30, 115, 30));
        sizePolicy.setHeightForWidth(lineEditCurrentTime->sizePolicy().hasHeightForWidth());
        lineEditCurrentTime->setSizePolicy(sizePolicy);
        lineEditCurrentTime->setMinimumSize(QSize(100, 10));
        lineEditCurrentTime->setMaximumSize(QSize(115, 30));
        lineEditCurrentTime->setFont(font1);
        lineEditCurrentTime->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"	color:rgb(36, 31, 49);\n"
"    border: 2px solid gray;\n"
"    border-radius: 10px;\n"
"    padding: 0 8px;\n"
"    background: white;\n"
"    selection-background-color: darkgray;\n"
"}"));
        lineEditCurrentTime->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        lineEditCurrentTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEditCurrentTime->setReadOnly(true);
        groupBox_24 = new QGroupBox(groupBox_8);
        groupBox_24->setObjectName(QString::fromUtf8("groupBox_24"));
        groupBox_24->setGeometry(QRect(10, 100, 211, 71));
        groupBox_24->setAlignment(Qt::AlignCenter);
        horizontalSliderLightValue = new QSlider(groupBox_24);
        horizontalSliderLightValue->setObjectName(QString::fromUtf8("horizontalSliderLightValue"));
        horizontalSliderLightValue->setGeometry(QRect(10, 30, 191, 31));
        horizontalSliderLightValue->setStyleSheet(QString::fromUtf8(".QSlider {\n"
"    min-height: 12px;\n"
"    max-height: 68px;\n"
"    background: #060b0f ;\n"
"}\n"
"\n"
"\n"
".QSlider::groove:horizontal {\n"
"    border: 1px solid rgb(94, 92, 100);\n"
"    border-radius: 5px;\n"
"    height: 25px;\n"
"    background: rgb(26, 95, 180);\n"
"    margin: 0 12px;\n"
"}\n"
"\n"
".QSlider::handle:horizontal {\n"
"    background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);\n"
"    border: 1px solid #5c5c5c;\n"
"    width: 24px;\n"
"    margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */\n"
"    border-radius: 3px;\n"
"}"));
        horizontalSliderLightValue->setMaximum(4000);
        horizontalSliderLightValue->setOrientation(Qt::Horizontal);
        groupBox_14 = new QGroupBox(groupBox_8);
        groupBox_14->setObjectName(QString::fromUtf8("groupBox_14"));
        groupBox_14->setGeometry(QRect(230, 120, 141, 71));
        groupBox_14->setAlignment(Qt::AlignCenter);
        lineEditCurrentLight = new QLineEdit(groupBox_14);
        lineEditCurrentLight->setObjectName(QString::fromUtf8("lineEditCurrentLight"));
        lineEditCurrentLight->setGeometry(QRect(10, 30, 115, 30));
        sizePolicy.setHeightForWidth(lineEditCurrentLight->sizePolicy().hasHeightForWidth());
        lineEditCurrentLight->setSizePolicy(sizePolicy);
        lineEditCurrentLight->setMinimumSize(QSize(100, 10));
        lineEditCurrentLight->setMaximumSize(QSize(115, 30));
        lineEditCurrentLight->setFont(font1);
        lineEditCurrentLight->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"	color:rgb(36, 31, 49);\n"
"    border: 2px solid gray;\n"
"    border-radius: 10px;\n"
"    padding: 0 8px;\n"
"    background: white;\n"
"    selection-background-color: darkgray;\n"
"}"));
        lineEditCurrentLight->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        lineEditCurrentLight->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEditCurrentLight->setReadOnly(true);
        groupBox_20 = new QGroupBox(centralwidget);
        groupBox_20->setObjectName(QString::fromUtf8("groupBox_20"));
        groupBox_20->setGeometry(QRect(680, 0, 331, 181));
        groupBox_20->setFont(font);
        groupBox_20->setAlignment(Qt::AlignCenter);
        groupBox_20->setFlat(false);
        groupBox_44 = new QGroupBox(groupBox_20);
        groupBox_44->setObjectName(QString::fromUtf8("groupBox_44"));
        groupBox_44->setGeometry(QRect(10, 30, 151, 61));
        groupBox_44->setAlignment(Qt::AlignCenter);
        lineEditPowerSource = new QLineEdit(groupBox_44);
        lineEditPowerSource->setObjectName(QString::fromUtf8("lineEditPowerSource"));
        lineEditPowerSource->setGeometry(QRect(20, 30, 121, 27));
        sizePolicy.setHeightForWidth(lineEditPowerSource->sizePolicy().hasHeightForWidth());
        lineEditPowerSource->setSizePolicy(sizePolicy);
        lineEditPowerSource->setMinimumSize(QSize(100, 10));
        lineEditPowerSource->setMaximumSize(QSize(200, 30));
        lineEditPowerSource->setFont(font1);
        lineEditPowerSource->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"	color:rgb(36, 31, 49);\n"
"    border: 2px solid gray;\n"
"    border-radius: 10px;\n"
"    padding: 0 8px;\n"
"    background: white;\n"
"    selection-background-color: darkgray;\n"
"}"));
        lineEditPowerSource->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        lineEditPowerSource->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEditPowerSource->setReadOnly(true);
        groupBox_45 = new QGroupBox(groupBox_20);
        groupBox_45->setObjectName(QString::fromUtf8("groupBox_45"));
        groupBox_45->setGeometry(QRect(160, 30, 161, 61));
        groupBox_45->setAlignment(Qt::AlignCenter);
        lineEditBaterryVoltage = new QLineEdit(groupBox_45);
        lineEditBaterryVoltage->setObjectName(QString::fromUtf8("lineEditBaterryVoltage"));
        lineEditBaterryVoltage->setGeometry(QRect(20, 30, 121, 27));
        sizePolicy.setHeightForWidth(lineEditBaterryVoltage->sizePolicy().hasHeightForWidth());
        lineEditBaterryVoltage->setSizePolicy(sizePolicy);
        lineEditBaterryVoltage->setMinimumSize(QSize(100, 10));
        lineEditBaterryVoltage->setMaximumSize(QSize(200, 30));
        lineEditBaterryVoltage->setFont(font1);
        lineEditBaterryVoltage->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"	color:rgb(36, 31, 49);\n"
"    border: 2px solid gray;\n"
"    border-radius: 10px;\n"
"    padding: 0 8px;\n"
"    background: white;\n"
"    selection-background-color: darkgray;\n"
"}"));
        lineEditBaterryVoltage->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        lineEditBaterryVoltage->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEditBaterryVoltage->setReadOnly(true);
        groupBoxBatteryLevel = new QGroupBox(groupBox_20);
        groupBoxBatteryLevel->setObjectName(QString::fromUtf8("groupBoxBatteryLevel"));
        groupBoxBatteryLevel->setGeometry(QRect(10, 90, 311, 81));
        sizePolicy.setHeightForWidth(groupBoxBatteryLevel->sizePolicy().hasHeightForWidth());
        groupBoxBatteryLevel->setSizePolicy(sizePolicy);
        groupBoxBatteryLevel->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        gridLayout_8 = new QGridLayout(groupBoxBatteryLevel);
        gridLayout_8->setSpacing(0);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        gridLayout_8->setContentsMargins(10, 10, 10, 10);
        progressBarBattery = new QProgressBar(groupBoxBatteryLevel);
        progressBarBattery->setObjectName(QString::fromUtf8("progressBarBattery"));
        sizePolicy.setHeightForWidth(progressBarBattery->sizePolicy().hasHeightForWidth());
        progressBarBattery->setSizePolicy(sizePolicy);
        progressBarBattery->setStyleSheet(QString::fromUtf8("QProgressBar {\n"
"    border: 2px solid rgb(94, 92, 100);\n"
"    border-radius: 5px;\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"	background-color: rgb(26, 95, 180);\n"
"    width: 20px;\n"
"}"));
        progressBarBattery->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        progressBarBattery->setValue(24);
        progressBarBattery->setAlignment(Qt::AlignCenter);
        progressBarBattery->setTextVisible(false);
        progressBarBattery->setInvertedAppearance(false);

        gridLayout_8->addWidget(progressBarBattery, 0, 0, 1, 1);

        groupBox_21 = new QGroupBox(centralwidget);
        groupBox_21->setObjectName(QString::fromUtf8("groupBox_21"));
        groupBox_21->setGeometry(QRect(800, 180, 221, 101));
        groupBox_21->setAlignment(Qt::AlignCenter);
        pushButtonLightAuto = new QPushButton(groupBox_21);
        pushButtonLightAuto->setObjectName(QString::fromUtf8("pushButtonLightAuto"));
        pushButtonLightAuto->setGeometry(QRect(10, 30, 101, 31));
        pushButtonLightManual = new QPushButton(groupBox_21);
        pushButtonLightManual->setObjectName(QString::fromUtf8("pushButtonLightManual"));
        pushButtonLightManual->setGeometry(QRect(10, 60, 101, 31));
        pushButtonLightOn = new QPushButton(groupBox_21);
        pushButtonLightOn->setObjectName(QString::fromUtf8("pushButtonLightOn"));
        pushButtonLightOn->setGeometry(QRect(110, 30, 101, 31));
        pushButtonLightOff = new QPushButton(groupBox_21);
        pushButtonLightOff->setObjectName(QString::fromUtf8("pushButtonLightOff"));
        pushButtonLightOff->setGeometry(QRect(110, 60, 101, 31));
        groupBox_22 = new QGroupBox(centralwidget);
        groupBox_22->setObjectName(QString::fromUtf8("groupBox_22"));
        groupBox_22->setGeometry(QRect(800, 280, 221, 111));
        groupBox_22->setAlignment(Qt::AlignCenter);
        groupBox_28 = new QGroupBox(groupBox_22);
        groupBox_28->setObjectName(QString::fromUtf8("groupBox_28"));
        groupBox_28->setGeometry(QRect(270, 110, 321, 121));
        groupBox_28->setAlignment(Qt::AlignCenter);
        pushButtonHeaterAuto = new QPushButton(groupBox_22);
        pushButtonHeaterAuto->setObjectName(QString::fromUtf8("pushButtonHeaterAuto"));
        pushButtonHeaterAuto->setGeometry(QRect(10, 40, 101, 31));
        pushButtonHeaterManual = new QPushButton(groupBox_22);
        pushButtonHeaterManual->setObjectName(QString::fromUtf8("pushButtonHeaterManual"));
        pushButtonHeaterManual->setGeometry(QRect(10, 70, 101, 31));
        pushButtonHeaterOn = new QPushButton(groupBox_22);
        pushButtonHeaterOn->setObjectName(QString::fromUtf8("pushButtonHeaterOn"));
        pushButtonHeaterOn->setGeometry(QRect(110, 70, 101, 31));
        pushButtonHeaterOff = new QPushButton(groupBox_22);
        pushButtonHeaterOff->setObjectName(QString::fromUtf8("pushButtonHeaterOff"));
        pushButtonHeaterOff->setGeometry(QRect(110, 40, 101, 31));
        groupBox_31 = new QGroupBox(centralwidget);
        groupBox_31->setObjectName(QString::fromUtf8("groupBox_31"));
        groupBox_31->setGeometry(QRect(800, 390, 221, 71));
        groupBox_31->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);
        pushButtonFilterOn = new QPushButton(groupBox_31);
        pushButtonFilterOn->setObjectName(QString::fromUtf8("pushButtonFilterOn"));
        pushButtonFilterOn->setGeometry(QRect(110, 30, 101, 31));
        pushButtonFilterOff = new QPushButton(groupBox_31);
        pushButtonFilterOff->setObjectName(QString::fromUtf8("pushButtonFilterOff"));
        pushButtonFilterOff->setGeometry(QRect(10, 30, 101, 31));
        groupBox_18 = new QGroupBox(centralwidget);
        groupBox_18->setObjectName(QString::fromUtf8("groupBox_18"));
        groupBox_18->setGeometry(QRect(590, 360, 201, 101));
        groupBox_18->setFont(font);
        groupBox_18->setAlignment(Qt::AlignCenter);
        groupBox_18->setFlat(false);
        groupBox_19 = new QGroupBox(groupBox_18);
        groupBox_19->setObjectName(QString::fromUtf8("groupBox_19"));
        groupBox_19->setGeometry(QRect(30, 30, 141, 61));
        groupBox_19->setAlignment(Qt::AlignCenter);
        lineEditWaterLevel = new QLineEdit(groupBox_19);
        lineEditWaterLevel->setObjectName(QString::fromUtf8("lineEditWaterLevel"));
        lineEditWaterLevel->setGeometry(QRect(10, 30, 115, 27));
        sizePolicy.setHeightForWidth(lineEditWaterLevel->sizePolicy().hasHeightForWidth());
        lineEditWaterLevel->setSizePolicy(sizePolicy);
        lineEditWaterLevel->setMinimumSize(QSize(100, 10));
        lineEditWaterLevel->setMaximumSize(QSize(115, 30));
        lineEditWaterLevel->setFont(font1);
        lineEditWaterLevel->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"	color:rgb(36, 31, 49);\n"
"    border: 2px solid gray;\n"
"    border-radius: 10px;\n"
"    padding: 0 8px;\n"
"    background: white;\n"
"    selection-background-color: darkgray;\n"
"}"));
        lineEditWaterLevel->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        lineEditWaterLevel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEditWaterLevel->setReadOnly(true);
        groupBoxTextLogs = new QGroupBox(centralwidget);
        groupBoxTextLogs->setObjectName(QString::fromUtf8("groupBoxTextLogs"));
        groupBoxTextLogs->setGeometry(QRect(10, 354, 499, 121));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBoxTextLogs->sizePolicy().hasHeightForWidth());
        groupBoxTextLogs->setSizePolicy(sizePolicy1);
        groupBoxTextLogs->setMaximumSize(QSize(16777215, 1000));
        groupBoxTextLogs->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        gridLayout_5 = new QGridLayout(groupBoxTextLogs);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(10, 10, 10, 10);
        textEditLogs = new QTextEdit(groupBoxTextLogs);
        textEditLogs->setObjectName(QString::fromUtf8("textEditLogs"));
        textEditLogs->setEnabled(true);
        sizePolicy.setHeightForWidth(textEditLogs->sizePolicy().hasHeightForWidth());
        textEditLogs->setSizePolicy(sizePolicy);
        textEditLogs->setMaximumSize(QSize(477, 70));
        textEditLogs->setStyleSheet(QString::fromUtf8("QTextEdit#textEditLogs { color: white; background-color: rgb(36, 31, 49);selection-background-color: rgb(233, 99, 0); }\n"
""));
        textEditLogs->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        textEditLogs->setTextInteractionFlags(Qt::NoTextInteraction);

        gridLayout_5->addWidget(textEditLogs, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1024, 27));
        QFont font2;
        font2.setBold(true);
        font2.setWeight(75);
        menubar->setFont(font2);
        menubar->setAutoFillBackground(false);
        menubar->setStyleSheet(QString::fromUtf8("QMenuBar#menubar {\n"
"	background-color: rgb(130, 130, 130);\n"
"	selection-background-color: rgb(153, 193, 241);\n"
"}\n"
"\n"
"QMenu#menuConnection {\n"
"	background-color: rgb(130, 130, 130);\n"
"	selection-background-color: rgb(153, 193, 241);\n"
"}\n"
"\n"
"QMenu#menuFile {\n"
"	background-color: rgb(130, 130, 130);\n"
"	selection-background-color: rgb(153, 193, 241);\n"
"}\n"
"\n"
"QMenu#menuLanguage {\n"
"	background-color: rgb(130, 130, 130);\n"
"	selection-background-color: rgb(153, 193, 241);\n"
"}"));
        menubar->setLocale(QLocale(QLocale::Polish, QLocale::Poland));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        QFont font3;
        font3.setBold(false);
        font3.setWeight(50);
        menuFile->setFont(font3);
        menuConnection = new QMenu(menubar);
        menuConnection->setObjectName(QString::fromUtf8("menuConnection"));
        menuConnect = new QMenu(menuConnection);
        menuConnect->setObjectName(QString::fromUtf8("menuConnect"));
        menuConnect->setFocusPolicy(Qt::NoFocus);
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        statusbar->setStyleSheet(QString::fromUtf8("QStatusBar#statusbar{\n"
"	background-color: rgb(130, 130, 130);\n"
"	margin-right: 0px;\n"
"	padding: 6px;\n"
"}"));
        statusbar->setSizeGripEnabled(false);
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuConnection->menuAction());
        menuFile->addAction(actionClose);
        menuConnection->addAction(actionSearch);
        menuConnection->addAction(menuConnect->menuAction());
        menuConnection->addAction(actionDisconnect);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Roomba App", nullptr));
        actionClose->setText(QCoreApplication::translate("MainWindow", "Zamknij", nullptr));
#if QT_CONFIG(tooltip)
        actionClose->setToolTip(QCoreApplication::translate("MainWindow", "Zamknij", nullptr));
#endif // QT_CONFIG(tooltip)
        actionDisconnect->setText(QCoreApplication::translate("MainWindow", "Roz\305\202\304\205cz", nullptr));
#if QT_CONFIG(tooltip)
        actionDisconnect->setToolTip(QCoreApplication::translate("MainWindow", "Roz\305\202\304\205cz", nullptr));
#endif // QT_CONFIG(tooltip)
        actionSearch->setText(QCoreApplication::translate("MainWindow", "Szukaj", nullptr));
#if QT_CONFIG(tooltip)
        actionSearch->setToolTip(QCoreApplication::translate("MainWindow", "Szukaj", nullptr));
#endif // QT_CONFIG(tooltip)
        actionPolish->setText(QCoreApplication::translate("MainWindow", "Polski", nullptr));
        actionEnglish_US->setText(QCoreApplication::translate("MainWindow", "Angielski (US)", nullptr));
        serialStatus->setText(QCoreApplication::translate("MainWindow", "Roz\305\202\304\205czono z portem szeregowym", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "Grza\305\202ka", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MainWindow", "Moc grza\305\202ki ", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("MainWindow", "Wydzielana moc [W]", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("MainWindow", "Stan", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("MainWindow", "Temperatura [*C]", nullptr));
        groupBox_6->setTitle(QCoreApplication::translate("MainWindow", "Zadana", nullptr));
        groupBox_7->setTitle(QCoreApplication::translate("MainWindow", "Aktualna", nullptr));
        groupBox_23->setTitle(QCoreApplication::translate("MainWindow", "Temperatura", nullptr));
        groupBox_8->setTitle(QCoreApplication::translate("MainWindow", "O\305\233wietlenie", nullptr));
        groupBox_9->setTitle(QCoreApplication::translate("MainWindow", "Moc o\305\233wietlenia (tryb manual)", nullptr));
        groupBox_12->setTitle(QCoreApplication::translate("MainWindow", "Nat\304\231\305\274enie \305\233wiat\305\202a [lx]", nullptr));
        groupBox_13->setTitle(QCoreApplication::translate("MainWindow", "Zadana", nullptr));
        groupBox_15->setTitle(QCoreApplication::translate("MainWindow", "Godzina", nullptr));
        groupBox_16->setTitle(QCoreApplication::translate("MainWindow", "W\305\202\304\205czenia", nullptr));
        groupBox_17->setTitle(QCoreApplication::translate("MainWindow", "Wy\305\202\304\205czenia", nullptr));
        groupBox_10->setTitle(QCoreApplication::translate("MainWindow", "Aktualna Godz.", nullptr));
        groupBox_24->setTitle(QCoreApplication::translate("MainWindow", "Nat\304\231\305\274enie", nullptr));
        groupBox_14->setTitle(QCoreApplication::translate("MainWindow", "Aktualna", nullptr));
        groupBox_20->setTitle(QCoreApplication::translate("MainWindow", "Zasilanie", nullptr));
        groupBox_44->setTitle(QCoreApplication::translate("MainWindow", "\305\271r\303\263d\305\202o zasialnia", nullptr));
        groupBox_45->setTitle(QCoreApplication::translate("MainWindow", "Napi\304\231cie akumulatora", nullptr));
        groupBoxBatteryLevel->setTitle(QCoreApplication::translate("MainWindow", "Poziom na\305\202adowania akumulatora", nullptr));
        groupBox_21->setTitle(QCoreApplication::translate("MainWindow", "O\305\233wietlenie", nullptr));
        pushButtonLightAuto->setText(QCoreApplication::translate("MainWindow", "Auto", nullptr));
        pushButtonLightManual->setText(QCoreApplication::translate("MainWindow", "Manual", nullptr));
        pushButtonLightOn->setText(QCoreApplication::translate("MainWindow", "W\305\202\304\205cz", nullptr));
        pushButtonLightOff->setText(QCoreApplication::translate("MainWindow", "Wy\305\202\304\205cz", nullptr));
        groupBox_22->setTitle(QCoreApplication::translate("MainWindow", "Grza\305\202ka", nullptr));
        groupBox_28->setTitle(QCoreApplication::translate("MainWindow", "Godzina", nullptr));
        pushButtonHeaterAuto->setText(QCoreApplication::translate("MainWindow", "Auto", nullptr));
        pushButtonHeaterManual->setText(QCoreApplication::translate("MainWindow", "Manual", nullptr));
        pushButtonHeaterOn->setText(QCoreApplication::translate("MainWindow", "W\305\202\304\205cz", nullptr));
        pushButtonHeaterOff->setText(QCoreApplication::translate("MainWindow", "Wy\305\202\304\205cz", nullptr));
        groupBox_31->setTitle(QCoreApplication::translate("MainWindow", "Filtr", nullptr));
        pushButtonFilterOn->setText(QCoreApplication::translate("MainWindow", "W\305\202\304\205cz", nullptr));
        pushButtonFilterOff->setText(QCoreApplication::translate("MainWindow", "Wy\305\202\304\205cz", nullptr));
        groupBox_18->setTitle(QCoreApplication::translate("MainWindow", "Poziom wody", nullptr));
        groupBox_19->setTitle(QCoreApplication::translate("MainWindow", "Poziom [cm]", nullptr));
        groupBoxTextLogs->setTitle(QCoreApplication::translate("MainWindow", "Informacje komunikacyjne", nullptr));
        menuFile->setTitle(QCoreApplication::translate("MainWindow", "Plik", nullptr));
        menuConnection->setTitle(QCoreApplication::translate("MainWindow", "Po\305\202\304\205czenie", nullptr));
        menuConnect->setTitle(QCoreApplication::translate("MainWindow", "Po\305\202\304\205cz", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
