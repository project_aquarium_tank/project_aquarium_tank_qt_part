/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.hh'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../aquarium/inc/mainwindow.hh"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.hh' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[34];
    char stringdata0[529];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 24), // "on_actionClose_triggered"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 29), // "on_actionDisconnect_triggered"
QT_MOC_LITERAL(4, 67, 25), // "on_actionSearch_triggered"
QT_MOC_LITERAL(5, 93, 19), // "connectSerialDevice"
QT_MOC_LITERAL(6, 113, 10), // "actionName"
QT_MOC_LITERAL(7, 124, 13), // "serialTimeout"
QT_MOC_LITERAL(8, 138, 16), // "showErrorMessage"
QT_MOC_LITERAL(9, 155, 7), // "message"
QT_MOC_LITERAL(10, 163, 16), // "addMessageToLogs"
QT_MOC_LITERAL(11, 180, 14), // "addHeaterPower"
QT_MOC_LITERAL(12, 195, 14), // "addHeaterState"
QT_MOC_LITERAL(13, 210, 10), // "addTempSet"
QT_MOC_LITERAL(14, 221, 14), // "addTempCurrent"
QT_MOC_LITERAL(15, 236, 14), // "addPowerSource"
QT_MOC_LITERAL(16, 251, 17), // "addBaterryVoltage"
QT_MOC_LITERAL(17, 269, 16), // "addBaterryCharge"
QT_MOC_LITERAL(18, 286, 11), // "addLightSet"
QT_MOC_LITERAL(19, 298, 15), // "addLightCurrent"
QT_MOC_LITERAL(20, 314, 14), // "addTimeCurrent"
QT_MOC_LITERAL(21, 329, 13), // "addWaterLevel"
QT_MOC_LITERAL(22, 343, 14), // "changeHeaterOn"
QT_MOC_LITERAL(23, 358, 15), // "changeHeaterOff"
QT_MOC_LITERAL(24, 374, 16), // "changeHeaterAuto"
QT_MOC_LITERAL(25, 391, 18), // "changeHeaterManual"
QT_MOC_LITERAL(26, 410, 13), // "changeLightOn"
QT_MOC_LITERAL(27, 424, 14), // "changeLightOff"
QT_MOC_LITERAL(28, 439, 15), // "changeLightAuto"
QT_MOC_LITERAL(29, 455, 17), // "changeLightManual"
QT_MOC_LITERAL(30, 473, 14), // "changeFilterOn"
QT_MOC_LITERAL(31, 488, 15), // "changeFilterOff"
QT_MOC_LITERAL(32, 504, 15), // "setSerialStatus"
QT_MOC_LITERAL(33, 520, 8) // "sendData"

    },
    "MainWindow\0on_actionClose_triggered\0"
    "\0on_actionDisconnect_triggered\0"
    "on_actionSearch_triggered\0connectSerialDevice\0"
    "actionName\0serialTimeout\0showErrorMessage\0"
    "message\0addMessageToLogs\0addHeaterPower\0"
    "addHeaterState\0addTempSet\0addTempCurrent\0"
    "addPowerSource\0addBaterryVoltage\0"
    "addBaterryCharge\0addLightSet\0"
    "addLightCurrent\0addTimeCurrent\0"
    "addWaterLevel\0changeHeaterOn\0"
    "changeHeaterOff\0changeHeaterAuto\0"
    "changeHeaterManual\0changeLightOn\0"
    "changeLightOff\0changeLightAuto\0"
    "changeLightManual\0changeFilterOn\0"
    "changeFilterOff\0setSerialStatus\0"
    "sendData"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      30,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  164,    2, 0x08 /* Private */,
       3,    0,  165,    2, 0x08 /* Private */,
       4,    0,  166,    2, 0x08 /* Private */,
       5,    1,  167,    2, 0x08 /* Private */,
       7,    0,  170,    2, 0x08 /* Private */,
       8,    1,  171,    2, 0x08 /* Private */,
      10,    1,  174,    2, 0x08 /* Private */,
      11,    0,  177,    2, 0x08 /* Private */,
      12,    1,  178,    2, 0x08 /* Private */,
      13,    0,  181,    2, 0x08 /* Private */,
      14,    1,  182,    2, 0x08 /* Private */,
      15,    1,  185,    2, 0x08 /* Private */,
      16,    1,  188,    2, 0x08 /* Private */,
      17,    1,  191,    2, 0x08 /* Private */,
      18,    0,  194,    2, 0x08 /* Private */,
      19,    1,  195,    2, 0x08 /* Private */,
      20,    0,  198,    2, 0x08 /* Private */,
      21,    1,  199,    2, 0x08 /* Private */,
      22,    0,  202,    2, 0x08 /* Private */,
      23,    0,  203,    2, 0x08 /* Private */,
      24,    0,  204,    2, 0x08 /* Private */,
      25,    0,  205,    2, 0x08 /* Private */,
      26,    0,  206,    2, 0x08 /* Private */,
      27,    0,  207,    2, 0x08 /* Private */,
      28,    0,  208,    2, 0x08 /* Private */,
      29,    0,  209,    2, 0x08 /* Private */,
      30,    0,  210,    2, 0x08 /* Private */,
      31,    0,  211,    2, 0x08 /* Private */,
      32,    1,  212,    2, 0x08 /* Private */,
      33,    0,  215,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_actionClose_triggered(); break;
        case 1: _t->on_actionDisconnect_triggered(); break;
        case 2: _t->on_actionSearch_triggered(); break;
        case 3: _t->connectSerialDevice((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->serialTimeout(); break;
        case 5: _t->showErrorMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->addMessageToLogs((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->addHeaterPower(); break;
        case 8: _t->addHeaterState((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->addTempSet(); break;
        case 10: _t->addTempCurrent((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->addPowerSource((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->addBaterryVoltage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: _t->addBaterryCharge((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: _t->addLightSet(); break;
        case 15: _t->addLightCurrent((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 16: _t->addTimeCurrent(); break;
        case 17: _t->addWaterLevel((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 18: _t->changeHeaterOn(); break;
        case 19: _t->changeHeaterOff(); break;
        case 20: _t->changeHeaterAuto(); break;
        case 21: _t->changeHeaterManual(); break;
        case 22: _t->changeLightOn(); break;
        case 23: _t->changeLightOff(); break;
        case 24: _t->changeLightAuto(); break;
        case 25: _t->changeLightManual(); break;
        case 26: _t->changeFilterOn(); break;
        case 27: _t->changeFilterOff(); break;
        case 28: _t->setSerialStatus((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 29: _t->sendData(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 30)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 30;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
