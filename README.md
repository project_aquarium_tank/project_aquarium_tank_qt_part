# Raspberry Pi Part

## Description
This project is part of the engineering work. The created device is a controller designed to operate devices in an aquarium. The QT framework was used to implement the interface, with the help of which the application shown in the image below was created. A touch screen was used to operate the device. Information to the STM32 L476RG-based controller is sent via a UART. 
![title](Images/interfejs_22_11_2022.png)
## Authors and acknowledgment
- Bartosz Wroński

## License
Copyright (c) 2022 Bartosz Wroński
MIT License